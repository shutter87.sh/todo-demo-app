# General

TODO is a simple task management app for the Android platform.

The user can create new todos. For each todo a name, a description and an end date can be added. All todos are shown in a list. Here the user is able to mark the todos as complete, change the order with drag-and-drop and use a swipe gesture to delete the todos. (When deleted, a so-called Snackbar is shown to undo the action.) For more clarity the end date is displayed in different colors, when already expired the color is red, when the todo is due today it is colored green.

When the user clicks on one todo a new screen is opened where he or she can change the details (name, description, end date). Furthermore the user is able to mark the todo as complete and delete it. (Before deleting a confirmation message is displayed to the user.)


# Video

The video demonstrates how the app works.   
[Youtube-Link](https://youtu.be/O-3w200VNB8)

# Installation

The apk-file (todo_v1.0.apk) can be downloaded to the computer and installed on a connected Android device.   
[Releases](https://gitlab.com/shutter87.sh/todo-demo-app/-/releases)

For installing, the Android Debug Bridge (adb) is needed.   
[Platform tools](https://developer.android.com/studio/releases/platform-tools)

The app can be installed with the following command `adb install -t app-debug.apk`.


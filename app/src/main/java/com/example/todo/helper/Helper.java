package com.example.todo.helper;

import android.content.SharedPreferences;
import android.content.res.Resources;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class Helper {

    public static int getPxFromDp(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int[] getYearMonthDay(long timestamp) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);

        int[] array = new int[3];

        array[0] = calendar.get(Calendar.YEAR);
        array[1] = calendar.get(Calendar.MONTH);
        array[2] = calendar.get(Calendar.DAY_OF_MONTH);

        return array;
    }

    public static long getTimestamp(int year, int month, int day) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTimeInMillis();
    }

    public static String getFormattedDate(long timestamp) {

        if (timestamp > 0) {

            return DateFormat.getDateInstance(DateFormat.MEDIUM)
                    .format(new Date(timestamp));
        }

        return "";
    }

    public static void putString(SharedPreferences sharedPreferences, String key, String value) {

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(key, value);
        editor.commit();
    }

    public static String getString(SharedPreferences sharedPreferences, String key) {
        return sharedPreferences.getString(key, null);
    }

    public static void deleteString(SharedPreferences sharedPreferences, String key) {

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.remove(key);
        editor.commit();
    }
}

package com.example.todo.presenter;

import android.content.SharedPreferences;

import com.example.todo.helper.Helper;
import com.example.todo.model.Todo;
import com.google.gson.Gson;

import java.util.UUID;

public class TodoDetailPresenter {

    private TodoDetailActivity todoDetailActivity;

    public TodoDetailPresenter(TodoDetailActivity todoDetailActivity) {
        this.todoDetailActivity = todoDetailActivity;
    }

    public void saveTodo(
            SharedPreferences sharedPreferences, String name, String description, long endTime) {

        Todo todo = new Todo();
        todo.setName(name);
        todo.setDescription(description);
        todo.setEndTime(endTime);
        todo.setUuid(UUID.randomUUID().toString());

        String json = new Gson().toJson(todo);
        Helper.putString(sharedPreferences, todo.getUuid(), json);

        todoDetailActivity.onTodoSaved(todo.getUuid());
    }

    public void loadTodo(SharedPreferences sharedPreferences, String uuid) {

        String value = Helper.getString(sharedPreferences, uuid);
        Todo todo = new Gson().fromJson(value, Todo.class);
        todo.setEndDate(Helper.getFormattedDate(todo.getEndTime()));

        todoDetailActivity.onTodoLoaded(todo);
    }

    public void saveChanges(SharedPreferences sharedPreferences, String name, String description,
                            long endTime, boolean completed, Todo todo) {

        todo.setName(name);
        todo.setDescription(description);
        todo.setEndTime(endTime);
        todo.setCompleted(completed);

        String json = new Gson().toJson(todo);
        Helper.putString(sharedPreferences, todo.getUuid(), json);

        todoDetailActivity.onChangesSaved();
    }
    
    public boolean isDataChanged(
            String name, String description, long endTime, boolean completed, Todo todo) {

        if (name.compareTo(todo.getName()) != 0) {
            return true;
        }

        if (description != null) {

            if (todo.getDescription() == null) {
                return true;
            }
            else if (description.compareTo(todo.getDescription()) != 0) {
                return true;
            }
        }

        if (Helper.getFormattedDate(endTime).compareTo(todo.getEndDate()) != 0) {
            return true;
        }

        if (completed != todo.isCompleted()) {
            return true;
        }

        return false;
    }

    public void deleteTodo(SharedPreferences sharedPreferences, String uuid) {

        Helper.deleteString(sharedPreferences, uuid);

        todoDetailActivity.onTodoDeleted();
    }

    public interface TodoDetailActivity {

        public void onTodoSaved(String uuid);
        public void onTodoLoaded(Todo todo);
        public void onChangesSaved();
        public void onTodoDeleted();
    }
}

package com.example.todo.presenter;

import android.content.SharedPreferences;

import com.example.todo.helper.Helper;
import com.example.todo.model.Todo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class TodoListPresenter {

    private TodoListActivity todoListActivity;

    public TodoListPresenter(TodoListActivity todoListActivity) {
        this.todoListActivity = todoListActivity;
    }

    public void loadTodos(SharedPreferences sharedPreferences) {

        List<Todo> todos = new ArrayList<>();

        String json = Helper.getString(sharedPreferences, "order");

        if (json == null) {

            todoListActivity.onTodosLoaded(todos);

            return;
        }

        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        List<String> uuids = new Gson().fromJson(json, type);

        for (String uuid : uuids) {

            String value = Helper.getString(sharedPreferences, uuid);
            Todo todo = new Gson().fromJson(value, Todo.class);
            todo.setEndDate(Helper.getFormattedDate(todo.getEndTime()));
            todos.add(todo);
        }

        todoListActivity.onTodosLoaded(todos);
    }

    public void loadNewTodo(SharedPreferences sharedPreferences, String uuid) {

        String value = Helper.getString(sharedPreferences, uuid);
        Todo todo = new Gson().fromJson(value, Todo.class);
        todo.setEndDate(Helper.getFormattedDate(todo.getEndTime()));

        todoListActivity.onNewTodoLoaded(todo);
    }

    public void loadChangedTodo(SharedPreferences sharedPreferences, String uuid) {

        String value = Helper.getString(sharedPreferences, uuid);

        if (value == null) {

            todoListActivity.onChangedTodoLoaded(null);

            return;
        }

        Todo todo = new Gson().fromJson(value, Todo.class);
        todo.setEndDate(Helper.getFormattedDate(todo.getEndTime()));

        todoListActivity.onChangedTodoLoaded(todo);
    }

    public void saveChanges(SharedPreferences sharedPreferences, Todo todo) {

        String json = new Gson().toJson(todo);
        Helper.putString(sharedPreferences, todo.getUuid(), json);
    }

    public void deleteTodo(SharedPreferences sharedPreferences, String uuid) {
        Helper.deleteString(sharedPreferences, uuid);
    }

    public void restoreTodo(SharedPreferences sharedPreferences, Todo todo) {

        String json = new Gson().toJson(todo);
        Helper.putString(sharedPreferences, todo.getUuid(), json);
    }

    public void saveOrder(SharedPreferences sharedPreferences, List<Todo> todos) {

        List<String> uuids = new ArrayList<>();

        for (Todo todo : todos) {
            uuids.add(todo.getUuid());
        }

        String json = new Gson().toJson(uuids);
        Helper.putString(sharedPreferences, "order", json);
    }

    public interface TodoListActivity {

        public void onTodosLoaded(List<Todo> todos);
        public void onNewTodoLoaded(Todo todo);
        public void onChangedTodoLoaded(Todo todo);
    }
}

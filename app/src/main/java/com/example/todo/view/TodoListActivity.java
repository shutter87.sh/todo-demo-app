package com.example.todo.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.todo.R;
import com.example.todo.databinding.ActivityTodoListBinding;
import com.example.todo.helper.Helper;
import com.example.todo.model.Todo;
import com.example.todo.presenter.TodoListPresenter;
import com.google.android.material.snackbar.Snackbar;

import java.util.Collections;
import java.util.List;

public class TodoListActivity extends AppCompatActivity
        implements TodoListPresenter.TodoListActivity, TodoListAdapter.OnItemClickListener,
        TodoListAdapter.OnCheckedChangeListener, ItemTouchHelperAdapter {

    private final int requestCodeCreateTodo = 0;
    private final int requestCodeEditTodo = 1;

    private ActivityTodoListBinding activityTodoListBinding;
    private TodoListPresenter todoListPresenter;
    private TodoListAdapter todoListAdapter;
    private int itemPosition;
    private Todo deletedTodo;
    private int deletedTodoPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityTodoListBinding
                = DataBindingUtil.setContentView(this, R.layout.activity_todo_list);

        todoListPresenter = new TodoListPresenter(this);
        todoListPresenter.loadTodos(getSharedPreferences("todos", MODE_PRIVATE));
    }

    @Override
    public void onTodosLoaded(List<Todo> todos) {

        activityTodoListBinding.recyclerViewTodos.setLayoutManager(
                new LinearLayoutManager(getApplicationContext()));

        activityTodoListBinding.recyclerViewTodos.addItemDecoration(
                new ListItemDecoration(Helper.getPxFromDp(8)));

        todoListAdapter = new TodoListAdapter(todos, this, this);
        activityTodoListBinding.recyclerViewTodos.setAdapter(todoListAdapter);

        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(this);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(activityTodoListBinding.recyclerViewTodos);

        activityTodoListBinding.floatingActionButtonCreateTodo
                .setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                //TODO: implement ActivityResultLauncher
                Intent intent = new Intent(getApplicationContext(), TodoDetailActivity.class);
                startActivityForResult(intent, requestCodeCreateTodo);
            }
        });
    }

    @Override
    public void onNewTodoLoaded(Todo todo) {

        todoListAdapter.getTodos().add(todo);
        todoListAdapter.notifyItemInserted(todoListAdapter.getTodos().size() - 1);

        todoListPresenter.saveOrder(
                getSharedPreferences("todos", MODE_PRIVATE), todoListAdapter.getTodos());
    }

    @Override
    public void onChangedTodoLoaded(Todo todo) {

        if (todo != null) {

            todoListAdapter.getTodos().set(itemPosition, todo);
            todoListAdapter.notifyItemChanged(itemPosition);
        }
        else {

            todoListAdapter.getTodos().remove(itemPosition);
            todoListAdapter.notifyItemRemoved(itemPosition);
            todoListAdapter.notifyItemRangeChanged(itemPosition, todoListAdapter.getItemCount());

            todoListPresenter.saveOrder(
                    getSharedPreferences("todos", MODE_PRIVATE), todoListAdapter.getTodos());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == requestCodeCreateTodo) && (resultCode == Activity.RESULT_OK)) {

            todoListPresenter.loadNewTodo(getSharedPreferences("todos", MODE_PRIVATE),
                    data.getStringExtra("uuid"));
        }
        else if ((requestCode == requestCodeEditTodo) && (resultCode == Activity.RESULT_OK)) {

            todoListPresenter.loadChangedTodo(getSharedPreferences("todos", MODE_PRIVATE),
                    todoListAdapter.getTodos().get(itemPosition).getUuid());
        }
    }

    @Override
    public void onItemClick(String uuid, int itemPosition) {

        this.itemPosition = itemPosition;

        //TODO: implement ActivityResultLauncher
        Intent intent = new Intent(getApplicationContext(), TodoDetailActivity.class);
        intent.putExtra("uuid", uuid);

        startActivityForResult(intent, requestCodeEditTodo);
    }

    @Override
    public void onCheckedChange(boolean completed, int itemPosition) {

        Todo todo = todoListAdapter.getTodos().get(itemPosition);
        todo.setCompleted(completed);

        boolean update = todo.updateEndDate();

        if (update) {
            todoListAdapter.notifyItemChanged(itemPosition);
        }

        todoListPresenter.saveChanges(getSharedPreferences("todos", MODE_PRIVATE), todo);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {

        if (fromPosition < toPosition) {

            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(todoListAdapter.getTodos(), i, i + 1);
            }
        }
        else {

            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(todoListAdapter.getTodos(), i, i - 1);
            }
        }

        todoListAdapter.notifyItemMoved(fromPosition, toPosition);

        todoListPresenter.saveOrder(
                getSharedPreferences("todos", MODE_PRIVATE), todoListAdapter.getTodos());

        return true;
    }

    @Override
    public void onItemDismiss(int position) {

        deletedTodo = todoListAdapter.getTodos().get(position);
        deletedTodoPosition = position;

        todoListAdapter.getTodos().remove(position);
        todoListAdapter.notifyItemRemoved(position);

        todoListPresenter.deleteTodo(
                getSharedPreferences("todos", MODE_PRIVATE), deletedTodo.getUuid());

        todoListPresenter.saveOrder(
                getSharedPreferences("todos", MODE_PRIVATE), todoListAdapter.getTodos());

        Snackbar snackbar
                = Snackbar.make(activityTodoListBinding.frameLayoutRoot,
                getResources().getString(R.string.deleted),
                Snackbar.LENGTH_LONG);

        snackbar.setAction(getResources().getString(R.string.undo),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        todoListAdapter.getTodos().add(deletedTodoPosition, deletedTodo);
                        todoListAdapter.notifyItemInserted(deletedTodoPosition);

                        todoListPresenter.restoreTodo(
                                getSharedPreferences("todos", MODE_PRIVATE), deletedTodo);

                        todoListPresenter.saveOrder(
                                getSharedPreferences("todos", MODE_PRIVATE),
                                todoListAdapter.getTodos());
                    }
                });

        snackbar.show();
    }
}
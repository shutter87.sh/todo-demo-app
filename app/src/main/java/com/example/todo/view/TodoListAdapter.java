package com.example.todo.view;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.todo.R;
import com.example.todo.databinding.ListItemTodoBinding;
import com.example.todo.model.Todo;

import java.util.List;

public class TodoListAdapter extends RecyclerView.Adapter<ListItemTodoViewHolder> {

    private List<Todo> todos;
    private OnItemClickListener onItemClickListener;
    private OnCheckedChangeListener onCheckedChangeListener;

    public TodoListAdapter(List<Todo> todos, OnItemClickListener onItemClickListener,
                           OnCheckedChangeListener onCheckedChangeListener) {

        this.todos = todos;
        this.onItemClickListener = onItemClickListener;
        this.onCheckedChangeListener = onCheckedChangeListener;
    }

    @NonNull
    @Override
    public ListItemTodoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ListItemTodoBinding listItemTodoBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()), R.layout.list_item_todo, parent, false);

        return new ListItemTodoViewHolder(listItemTodoBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListItemTodoViewHolder holder, int position) {

        holder.getListItemTodoBinding().setTodo(todos.get(holder.getAdapterPosition()));
        holder.getListItemTodoBinding().setOnItemClickListener(onItemClickListener);
        holder.getListItemTodoBinding().setListItemTodoViewHolder(holder);

        holder.getListItemTodoBinding().checkBox.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {

                    @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                        if (compoundButton.isPressed()) {
                            onCheckedChangeListener.onCheckedChange(b, holder.getAdapterPosition());
                        }
            }
        });
    }

    @Override
    public int getItemCount() {
        return todos.size();
    }

    public List<Todo> getTodos() {
        return todos;
    }

    public interface OnItemClickListener {

        public void onItemClick(String uuid, int itemPosition);
    }

    public interface OnCheckedChangeListener {

        public void onCheckedChange(boolean completed, int itemPosition);
    }
}

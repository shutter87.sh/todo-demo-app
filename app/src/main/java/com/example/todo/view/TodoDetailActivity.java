package com.example.todo.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.todo.R;
import com.example.todo.databinding.ActivityTodoDetailBinding;
import com.example.todo.helper.Helper;
import com.example.todo.model.Todo;
import com.example.todo.presenter.TodoDetailPresenter;
import com.google.android.material.snackbar.Snackbar;

public class TodoDetailActivity extends AppCompatActivity
        implements TodoDetailPresenter.TodoDetailActivity, DatePickerFragment.OnDateSetListener,
        ConfirmDeletingFragment.OnDeletingConfirmedListener {

    private ActivityTodoDetailBinding activityTodoDetailBinding;

    private TodoDetailPresenter todoDetailPresenter;

    private String name;
    private String description;
    private long endTime;
    private boolean completed;
    private Todo todo;
    private String uuid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityTodoDetailBinding
                = DataBindingUtil.setContentView(this, R.layout.activity_todo_detail);

        todoDetailPresenter = new TodoDetailPresenter(this);

        uuid = getIntent().getStringExtra("uuid");

        if (uuid != null) {
            todoDetailPresenter.loadTodo(getSharedPreferences("todos", MODE_PRIVATE), uuid);
        }
        else {
            createUi(getResources().getString(R.string.create_todo));
        }
    }

    private void createUi(String title) {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);

        activityTodoDetailBinding.textInputEditTextName.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                name = charSequence.toString();

                if (todo != null) {

                    if (todoDetailPresenter
                            .isDataChanged(name, description, endTime, completed, todo)) {

                        activityTodoDetailBinding.buttonSave.setVisibility(View.VISIBLE);
                        getSupportActionBar().setTitle(name);
                    }
                    else {
                        activityTodoDetailBinding.buttonSave.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        activityTodoDetailBinding.textInputEditTextDescription.addTextChangedListener(
                new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                description = charSequence.toString();

                if (todo != null) {

                    if (todoDetailPresenter
                            .isDataChanged(name, description, endTime, completed, todo)) {

                        activityTodoDetailBinding.buttonSave.setVisibility(View.VISIBLE);
                    }
                    else {
                        activityTodoDetailBinding.buttonSave.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        activityTodoDetailBinding.buttonDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                int[] array;

                if (endTime == 0) {
                    array = Helper.getYearMonthDay(System.currentTimeMillis());
                }
                else {
                    array = Helper.getYearMonthDay(endTime);
                }

                Bundle extras = new Bundle();
                extras.putInt("year", array[0]);
                extras.putInt("month", array[1]);
                extras.putInt("day", array[2]);

                DatePickerFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.setArguments(extras);
                datePickerFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });

        activityTodoDetailBinding.floatingActionButtonClearDate.setOnClickListener(
                new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                endTime = 0;

                activityTodoDetailBinding.buttonDate.setText(R.string.no_date);
                activityTodoDetailBinding.floatingActionButtonClearDate.setVisibility(View.GONE);

                if (todo != null) {

                    if (todoDetailPresenter
                            .isDataChanged(name, description, endTime, completed, todo)) {

                        activityTodoDetailBinding.buttonSave.setVisibility(View.VISIBLE);
                    }
                    else {
                        activityTodoDetailBinding.buttonSave.setVisibility(View.GONE);
                    }
                }
            }
        });

        activityTodoDetailBinding.buttonSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if ((name != null) && (name.length() > 0)) {

                    if (todo != null) {

                        todoDetailPresenter.saveChanges(getSharedPreferences("todos",
                                MODE_PRIVATE), name, description, endTime, completed, todo);
                    }
                    else {

                        todoDetailPresenter.saveTodo(getSharedPreferences("todos", MODE_PRIVATE),
                                name, description, endTime);
                    }
                }
                else {

                    Snackbar snackbar
                            = Snackbar.make(activityTodoDetailBinding.frameLayoutRoot,
                            getResources().getString(R.string.name_can_not_be_empty),
                            Snackbar.LENGTH_INDEFINITE);

                    snackbar.setAction(getResources().getString(R.string.ok),
                            new View.OnClickListener() {

                                @Override
                                public void onClick(View view) {

                                }
                            });

                    snackbar.show();
                }
            }
        });
    }

    @Override
    public void onDateSet(int year, int month, int day) {

        endTime = Helper.getTimestamp(year, month, day);

        activityTodoDetailBinding.buttonDate.setText(Helper.getFormattedDate(endTime));
        activityTodoDetailBinding.floatingActionButtonClearDate.setVisibility(View.VISIBLE);

        if (todo != null) {

            if (todoDetailPresenter.isDataChanged(name, description, endTime, completed, todo)) {
                activityTodoDetailBinding.buttonSave.setVisibility(View.VISIBLE);
            } else {
                activityTodoDetailBinding.buttonSave.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == android.R.id.home) {

            finish();

            return true;
        }
        else if (item.getItemId() == R.id.itemDelete) {

            ConfirmDeletingFragment confirmDeletingFragment = new ConfirmDeletingFragment();
            confirmDeletingFragment.show(getSupportFragmentManager(), "confirmDeleting");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTodoSaved(String uuid) {

        Intent intent = new Intent();
        intent.putExtra("uuid", uuid);

        setResult(Activity.RESULT_OK, intent);

        finish();
    }

    @Override
    public void onTodoLoaded(Todo todo) {

        this.todo = todo;
        this.name = todo.getName();
        this.description = todo.getDescription();
        this.completed = todo.isCompleted();

        activityTodoDetailBinding.textInputEditTextName.setText(name);
        activityTodoDetailBinding.textInputEditTextDescription.setText(description);

        if (todo.getEndDate().length() > 0) {

            activityTodoDetailBinding.buttonDate.setText(todo.getEndDate());
            activityTodoDetailBinding.floatingActionButtonClearDate.setVisibility(View.VISIBLE);
            endTime = todo.getEndTime();
        }

        activityTodoDetailBinding.buttonSave.setVisibility(View.GONE);
        activityTodoDetailBinding.linearLayoutCheckBox.setVisibility(View.VISIBLE);

        if (completed) {
            activityTodoDetailBinding.checkBox.setChecked(true);
        }

        activityTodoDetailBinding.checkBox.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {

                completed = checked;

                if (todoDetailPresenter.isDataChanged(name, description, endTime, completed,
                        TodoDetailActivity.this.todo)) {

                    activityTodoDetailBinding.buttonSave.setVisibility(View.VISIBLE);
                }
                else {
                    activityTodoDetailBinding.buttonSave.setVisibility(View.GONE);
                }
            }
        });

        createUi(name);
    }

    @Override
    public void onChangesSaved() {

        Intent intent = new Intent();

        setResult(Activity.RESULT_OK, intent);

        finish();
    }

    @Override
    public void onTodoDeleted() {

        Intent intent = new Intent();

        setResult(Activity.RESULT_OK, intent);

        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_todo_detail, menu);

        if (uuid == null) {
            menu.findItem(R.id.itemDelete).setVisible(false);
        }

        return true;
    }

    @Override
    public void onDeletingConfirmed() {
        todoDetailPresenter.deleteTodo(getSharedPreferences("todos", MODE_PRIVATE), uuid);
    }
}
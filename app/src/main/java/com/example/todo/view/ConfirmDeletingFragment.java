package com.example.todo.view;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.example.todo.R;

public class ConfirmDeletingFragment extends DialogFragment {

    private OnDeletingConfirmedListener onDeletingConfirmedListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(R.string.do_you_really_want_to_delete_the_todo)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onDeletingConfirmedListener.onDeletingConfirmed();
                    }
                })
                .setNegativeButton(R.string.cancel, null);

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            onDeletingConfirmedListener = (OnDeletingConfirmedListener) context;
        }
        catch (ClassCastException e) {

            throw new ClassCastException(
                    "Interface OnDeletingConfirmedListener is not implemented.");
        }
    }

    public interface OnDeletingConfirmedListener {

        public void onDeletingConfirmed();
    }
}

package com.example.todo.view;

import androidx.recyclerview.widget.RecyclerView;

import com.example.todo.databinding.ListItemTodoBinding;

public class ListItemTodoViewHolder extends RecyclerView.ViewHolder {

    private ListItemTodoBinding listItemTodoBinding;

    public ListItemTodoViewHolder(ListItemTodoBinding listItemTodoBinding) {
        super(listItemTodoBinding.getRoot());

        this.listItemTodoBinding = listItemTodoBinding;
    }

    public ListItemTodoBinding getListItemTodoBinding() {
        return listItemTodoBinding;
    }
}

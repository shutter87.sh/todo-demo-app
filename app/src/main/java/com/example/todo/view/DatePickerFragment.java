package com.example.todo.view;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private OnDateSetListener onDateSetListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        Bundle extras = getArguments();

        return new DatePickerDialog(getActivity(), this, extras.getInt("year"),
                extras.getInt("month"), extras.getInt("day"));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            onDateSetListener = (OnDateSetListener) context;
        }
        catch (ClassCastException e) {
            throw new ClassCastException("Interface OnDateSetListener is not implemented.");
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

        if (onDateSetListener != null) {
            onDateSetListener.onDateSet(year, month, day);
        }
    }

    public interface OnDateSetListener {

        public void onDateSet(int year, int month, int day);
    }
}

package com.example.todo.model;

import android.text.format.DateUtils;

import com.example.todo.R;

public class Todo {

    private String uuid;
    private String name;
    private String description;
    private long endTime;
    private boolean completed;

    private transient String endDate;
    private transient int endDateColor;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {

        this.endDate = endDate;

        if (!(completed)) {

            if (DateUtils.isToday(endTime)) {
                endDateColor = R.color.endDateToday;
            }
            else if (System.currentTimeMillis() <= endTime) {
                endDateColor = R.color.endDateDefault;
            }
            else {
                endDateColor = R.color.endDateOverdue;
            }
        }
        else {
            endDateColor = R.color.endDateDefault;
        }
    }

    public int getEndDateColor() {
        return endDateColor;
    }

    public boolean updateEndDate() {

        if (!(completed)) {

            if (DateUtils.isToday(endTime)) {

                if (endDateColor == R.color.endDateToday) {
                    return false;
                }

                endDateColor = R.color.endDateToday;

                return true;
            }
            else if (System.currentTimeMillis() <= endTime) {

                if (endDateColor == R.color.endDateDefault) {
                    return false;
                }

                endDateColor = R.color.endDateDefault;

                return true;
            }
            else {

                if (endDateColor == R.color.endDateOverdue) {
                    return false;
                }

                endDateColor = R.color.endDateOverdue;

                return true;
            }
        }
        else {

            if (endDateColor == R.color.endDateDefault) {
                return false;
            }

            endDateColor = R.color.endDateDefault;

            return true;
        }
    }
}
